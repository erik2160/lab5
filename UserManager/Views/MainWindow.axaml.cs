using System;
using System.Collections.ObjectModel;
using System.Linq;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using UserManager.Models;

namespace UserManager.Views;

public partial class MainWindow : Window
{
    private ObservableCollection<User> _usersList = new ObservableCollection<User>();
    private Notifications _notifications = new Notifications();

    public MainWindow()
    {
        InitializeComponent();
        UpdateTotalUsersText();
        ApplicationName.Text = "GERENCIAMENTO DE USUÁRIOS";
    }

    private void SubmitButton_OnClick(object? sender, RoutedEventArgs e)
    {
        _usersList.Add(new User(NameField.Text, LastnameField.Text, EmailField.Text));
        _notifications.NotifyUserAdded(NameField.Text);
        
        ClearFields();
        AddUsersListBox();
        UpdateTotalUsersText();
    }

    private void UpdateTotalUsersText()
    {
        var quantityOfUsers = _usersList.Count;
        string quantityOfUsersString = Convert.ToString(quantityOfUsers);
        TotalUsersField.Text = $"Quantidade de Usuários: {quantityOfUsersString}";
    }

    private void ClearFields()  
    {
        NameField.Clear();
        LastnameField.Clear();
        EmailField.Clear();
    }

    private bool IsFieldsEmpty()
    {
        if (string.IsNullOrEmpty(NameField.Text) || string.IsNullOrEmpty(LastnameField.Text) || string.IsNullOrEmpty(EmailField.Text))
        {
            return true;
        }
        return false;
    }

    private void AddUsersListBox()
    {
        UsersListBox.ItemsSource = _usersList;
    }

    private void UpdateUsersListBoxWithEmailSearched(User user)
    {
        UsersListBox.ItemsSource = new ObservableCollection<User>() { user };
        ReturnButton.IsVisible = true;
        TotalUsersField.Text = "E-mail encontrado";
        SearchEmailField.Clear();
    }

    private void NameField_OnTextChanged(object? sender, TextChangedEventArgs e)
    {
        UpdateSubmitButtonState();
    }

    private void LastnameField_OnTextChanged(object? sender, TextChangedEventArgs e)
    {
        UpdateSubmitButtonState();
    }

    private void EmailField_OnTextChanged(object? sender, TextChangedEventArgs e)
    {
        UpdateSubmitButtonState();
    }
    
    private void EmailField_OnKeyDown(object? sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
        {
            SubmitButton_OnClick(sender, e);
        } 
    }
    
    private void UpdateSubmitButtonState()
    {
        bool allFieldsFilled = !IsFieldsEmpty();
        SubmitButton.IsEnabled = allFieldsFilled;
    }

    private void SearchEmailButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var emailFieldText = SearchEmailField.Text;
        _notifications.NotifyUserSearched(emailFieldText);
        var filteredUsers = _usersList.Where(user => user.Email.Equals(emailFieldText));

        if (filteredUsers.Any())
        {
            foreach (var user in filteredUsers)
            {
                UpdateUsersListBoxWithEmailSearched(user);
            }
        }
        else
        {
            TotalUsersField.Text = "E-mail não encontrado";
            UsersListBox.ItemsSource = new ObservableCollection<User>();
            ReturnButton.IsVisible = true;
            SearchEmailField.Clear();
        }
    }

    private void ReturnButton_OnClick(object? sender, RoutedEventArgs e)
    {
        ReturnButton.IsVisible = false;
        AddUsersListBox();
        UpdateTotalUsersText();
    }

    private void AsButtonOrganizer_OnClick(object? sender, RoutedEventArgs e)
    {
        UsersListBox.ItemsSource = _usersList.OrderBy(user => user.Name);
        _notifications.NotifyListAsc();
    }

    private void DesButtonOrganizer_OnClick(object? sender, RoutedEventArgs e)
    {
        UsersListBox.ItemsSource = _usersList.OrderByDescending(user => user.Name);
        _notifications.NotifyListDesc();
    }
}