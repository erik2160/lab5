﻿namespace UserManager.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    private string Name { get; set; }
    private string LastName { get; set; }
    private string Email { get; set; }
}