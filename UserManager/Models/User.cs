namespace UserManager.Models;

public class User
{
    public string? Name { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }

    public User(string? name, string? lastName, string? email)
    {
        Name = name;
        LastName = lastName;
        Email = email;
    }

    public string? GetName()
    {
        return Name;
    }
    
    public string? GetLastName()
    {
        return LastName;
    }
    
    public string? GetEmail()
    {
        return Email;
    }

    public override string ToString()
    {
        // return $"NOME: {Name} | SOBRENOME: {LastName} | E-MAIL: {Email}";
        return $"Nome: {Name}\nSobrenome: {LastName}\nE-mail: {Email}";
    }
}