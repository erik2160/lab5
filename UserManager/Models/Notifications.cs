using System;

namespace UserManager.Models;

public class Notifications
{
    public void NotifyUserAdded(string username)
    {
        Console.WriteLine($"O Usuário {username} foi adicionado!");
    }

    public void NotifyUserSearched(string email)
    {
        Console.WriteLine($"O E-mail {email} foi pesquisado!");
    }

    public void NotifyListAsc()
    {
        Console.WriteLine("A lista foi organizada de forma Ascendente!");
    }

    public void NotifyListDesc()
    {
        Console.WriteLine("A lista foi organizada de forma Descendente!");
    }
}